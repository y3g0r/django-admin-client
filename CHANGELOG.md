# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.2 - 2019-03-03
### Fixed
- DjangoAdminBase.login when already logged-in.

### Added
- Ability to change objects (`DjangoAdminModel.change(object_id, fields)`)
- Helper properties in DjangoAdminModel (`path`, `spec`, `required_params`, `params`)
- Helper methods in DjangoAdminDynamic (`get_session`, `get_spec`, `get_base_client`)

### Changed
- DjangoAdminDynamic creation pattern
- DjangoAdminBase.post_with_csrf_and_auto_login improved errors detection
- DjangoAdminModel.add is more user friendly (no Exceptions,
allows omitting params, passing unknown params, returns errors from response)

### Removed
- DjangoAdminWithModels class

## 0.1 - 2019-03-01
### Added
- Generic Django Admin python client
- Feature to generate python project for specific Django Admin site

[Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/0.1...develop

