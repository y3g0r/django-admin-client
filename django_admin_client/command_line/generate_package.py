import json
import os
import sys
import textwrap
from getpass import getpass

from django_admin_client.helpers import NotSet
from django_admin_client.settings import settings
from django_admin_client import DjangoAdminBase, templates


def user_input(prompt, default=None, sensitive=False):
    if default and not sensitive and default is not NotSet:
        prompt = prompt.strip().rstrip(':') + ' (default: {})'.format(default)

    if not prompt.endswith(": "):
        prompt = prompt + ": "

    if sensitive:
        get_input = getpass
    else:
        get_input = input

    value = get_input(prompt) or default
    while not value:
        value = get_input(prompt)
    return value


def write_file(path, content):
    dir_path = os.path.dirname(path)
    try:
        os.makedirs(dir_path)
    except OSError as exc:
        pass

    with open(path, 'wt') as f:
        f.write(content)


def generate_model_methods(class_name, model_spec):
    methods = []

    add_params = []
    for param in model_spec['fields'].values():
        if not param['required'] and param['default_value'] is not None:
            param_str = "{name}: str = {default!r}".format(name=param['name'].replace('-', '_'), default=param['default_value'])
        elif not param['required']:
            param_str = "{name}: str = None".format(name=param['name'])
        else:
            param_str = "{name}: str".format(name=param['name'])
        add_params.append(param_str)

    add_params.sort(key=lambda s: "=" in s)

    add_params_str = ",\n".join(['self'] + add_params)

    add_item = []
    for param in model_spec['fields'].values():
        add_item.append("'{param_name}': {arg}".format(param_name=param['name'], arg=param['name'].replace('-', '_')))

    add_item_str = ',\n'.join(add_item)

    methods.append(
        textwrap.dedent("""\
        def add(
        {add_params}
        ):
            return self._model.add({{
        {item}
            }})
        """
        ).format(add_params=textwrap.indent(add_params_str, ' '*4),
                 item=textwrap.indent(add_item_str, (' '*4)*2))
    )

    return methods


def snake_to_camel_case(identifier):
    return "".join(map(str.capitalize, identifier.split('_')))


def model_class_name(app_name, model_id):
    app_name_camel = snake_to_camel_case(app_name)
    model_name_camel = snake_to_camel_case(model_id)
    return "{app_name}{model_name}Model".format(app_name=app_name_camel, model_name=model_name_camel)


def generate_model_class(model_spec):
    class_name = model_class_name(model_spec['app'], model_spec['id'])
    header = "class {class_name}({base_class}):".format(class_name=class_name, base_class='ModelBase')

    class_elements = [header]

    methods = generate_model_methods(class_name, model_spec=model_spec)

    if not methods:
        class_elements.append("\tpass\n")
    else:
        for method in methods:
            class_elements.append(textwrap.indent(method, ' '*4))

    class_text = "\n".join(class_elements)

    return {'reference': class_name, 'sourcecode': class_text}


def generate_model_classes(models_spec):
    model_classes = []
    for model in models_spec.values():
        model_classes.append(generate_model_class(model)['sourcecode'])

    return "\n\n".join(model_classes)


def app_class_name(app_name):
    app_name_camel = snake_to_camel_case(app_name)
    return "{app_name}App".format(app_name=app_name_camel)


def generate_app_class(app_name, models_specs):
    class_title = "class {class_name}:".format(class_name=app_class_name(app_name))

    models = []
    for model_spec in models_specs:
        models.append("self.{model_id} = {model_cls_name}(da.{model_id})".format(
            model_id=model_spec['id'],
            model_cls_name=model_class_name(app_name=app_name, model_id=model_spec['id'])
        ))

    models_str = "\n".join(models)

    init_method = textwrap.dedent("""\
        def __init__(self, da):
        {models}
    """).format(models=textwrap.indent(models_str, ' '*4))

    return "\n".join((class_title, textwrap.indent(init_method, ' '*4)))


def generate_app_classes(models_spec):
    apps_models_names = {}
    for model_spec in models_spec.values():
        apps_models_names.setdefault(model_spec['app'], []).append(model_spec)

    classes = []
    for app, models in apps_models_names.items():
        classes.append(generate_app_class(app, models))

    return "\n\n".join(classes)


def generate_client_class(spec, package_name):
    class_title = "class {name}DjangoAdminClient:".format(name=spec['name'])

    apps = {model['app'] for model in spec['models'].values()}

    apps_strings = []
    for app_name in apps:
        apps_strings.append("self.{app_name} = {app_cls_name}(self._da)".format(
            app_name=app_name,
            app_cls_name=app_class_name(app_name)
        ))

    apps_str = "\n".join(apps_strings)

    init_method = textwrap.dedent("""\
    def __init__(self, base_url, superuser_email, superuser_password):

        package = '{package_name}'
        path = 'spec.json'
        f = pkg_resources.resource_stream(package, path)
        spec = json.load(f)

        self._da = DjangoAdminDynamic.from_spec(
            base_url=base_url,
            superuser_email=superuser_email,
            superuser_password=superuser_password,
            spec=spec
        )
        
    {apps}
    """).format(package_name=package_name, apps=textwrap.indent(apps_str, ' '*4))

    return "\n".join((class_title, textwrap.indent(init_method, ' '*4)))


def generate_client(spec, package_name):
    header = textwrap.dedent("""\
        import json
        import pkg_resources
        
        from django_admin_client import DjangoAdminDynamic, DjangoAdminModel
    """)
    model_base_class = textwrap.dedent("""\
        class ModelBase:
            def __init__(self, model: DjangoAdminModel):
                self._model = model
        
            def all(self):
                return self._model.all()
        
            def find(self, query: str):
                return self._model.find(query)
        
            def add(self, **kwargs):
                return self._model.add(kwargs)
        
            def get(self, object_id: str):
                return self._model.get(object_id)
        
            def delete(self, object_id: str):
                return self._model.delete(object_id)
    """)
    models_classes = generate_model_classes(spec['models'])
    app_classes = generate_app_classes(spec['models'])
    client_class = generate_client_class(spec, package_name)
    client_pieces = [
        header,
        model_base_class,
        models_classes,
        app_classes,
        client_class,
    ]
    return "\n\n".join(client_pieces)


def main():
    base_url = user_input('Base url (including /admin)', default=settings.ADMIN_BASE_URL)
    su_email = user_input('Superuser username', default=settings.SUPERUSER_EMAIL)
    su_password = user_input('Superuser password', default=settings.SUPERUSER_PASSWORD, sensitive=True)
    site_name = user_input('Site name', default=settings.SITE_NAME).replace('-', '_')
    path_to_package = user_input('Path to package', default=settings.PATH_TO_PACKAGE)
    version = user_input('Version', default=settings.PACKAGE_VERSION)

    site_name = ''.join(site_name.replace('-', ' ').title().split())
    project_name = "-".join(site_name.split() + ['admin', 'client']).lower()
    package_name = "_".join(site_name.split() + ['admin', 'client']).lower()

    path_to_project = os.path.join(path_to_package, project_name)
    try:
        os.makedirs(path_to_project)
    except OSError as exc:
        overwrite = user_input("Creation of the directory '{path}' failed: '{exc}'. Try to overwrite?".format(path=path_to_project, exc=exc), default='y')
        if overwrite.lower().startswith('y'):
            pass
        else:
            sys.exit(1)

    da = DjangoAdminBase(base_url, su_email, su_password)
    spec = da.generate_spec()
    spec['version'] = version
    spec['name'] = site_name

    write_file(os.path.join(path_to_project, package_name, 'spec.json'), json.dumps(spec, indent=2))
    write_file(os.path.join(path_to_project, 'setup.py'), templates.setup_py.format(
        project_name=project_name,
        package=package_name,
        package_version=version,
        name=site_name,
    ))
    write_file(os.path.join(path_to_project, 'README.md'), templates.readme_md.format(project_name=project_name))
    write_file(os.path.join(path_to_project, package_name, '__init__.py'), templates.init_py.format(name=site_name))
    write_file(os.path.join(path_to_project, package_name, 'client.py'), generate_client(spec=spec, package_name=package_name))

    print("Generated package in {path}".format(path=path_to_project))
