import json
from getpass import getpass

from django_admin_client import DjangoAdminBase


def main():
    print('generating')

    base_url = input('Base url (including /admin): ')
    su_email = input('Superuser email: ')
    su_password = getpass('Superuser password: ')
    da = DjangoAdminBase(base_url, su_email, su_password)

    print(json.dumps(da.generate_spec(), indent=2))
